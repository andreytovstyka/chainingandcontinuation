﻿// <copyright file="ChainingAndContinuation.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ChainingAndContinuation
{
    /// <summary>
    /// Demonstration class of task "Chaining and Continuation".
    /// </summary>
    public static class ChainingAndContinuation
    {
        private static object ConsoleLock = new ();
        private static Random random = new ();

        /// <summary>
        /// Demonstration chaining and continuation task.
        /// </summary>
        /// <param name="arrayLenght">array length.</param>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue or array lenght less than one.</exception>
        /// <returns>average value of the antecedent array of integers.</returns>
        public static double Demonstration(int arrayLenght, int minRandomValue, int maxRandomValue)
        {
            Helpers.IfValueLessThanDefinedException(value: arrayLenght, edgeValue: 1, nameof(arrayLenght));
            Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

            Task<int[]> taskCreateArray = Task.Factory.StartNew(() => CreateIntArrayWithRandomElements(arrayLenght, minRandomValue, maxRandomValue));
            Task taskPrintCreatedArray = taskCreateArray.ContinueWith(antecedent => PrintArray(antecedent.Result, "Created array."));

            Task<int> taskRandomNumber = Task.Factory.StartNew<int>(() => GetRandomInteger(minRandomValue, maxRandomValue));
            Task taskPrintRandomInteger = taskRandomNumber.ContinueWith(antecedent => PrintMessage($"Generated random number {antecedent.Result}"));

            Task taskReadyForMultiple = Task.WhenAll(taskCreateArray, taskRandomNumber);
            Task<int[]> taskMultipliedArrayByNumber = taskReadyForMultiple.ContinueWith(t => MultipliesArrayByNumber(taskCreateArray.Result, taskRandomNumber.Result));
            Task taskPrintMultipliedArrayByNumber = taskMultipliedArrayByNumber.ContinueWith(antecedent => PrintArray(antecedent.Result, "Multiplied array."));

            Task<int[]> taskSortArray = taskMultipliedArrayByNumber.ContinueWith(antecedent => SortArrayByAscending(antecedent.Result));
            Task taskPrintSortedArray = taskSortArray.ContinueWith(antecedent => PrintArray(antecedent.Result, "Ordered array."));

            Task<double> taskCalculateAverage = taskMultipliedArrayByNumber.ContinueWith(antecedent => GetTheAverageOfTheElementsOfIntegerArray(antecedent.Result));
            Task taskPrintAverageValue = taskCalculateAverage.ContinueWith(antecedent => PrintMessage($"Average value {antecedent.Result}"));

            Task.WaitAll(
                taskPrintCreatedArray,
                taskPrintRandomInteger,
                taskPrintMultipliedArrayByNumber,
                taskPrintSortedArray,
                taskPrintAverageValue);

            return taskCalculateAverage.Result;
        }

        /// <summary>
        /// Create int array with random elements.
        /// </summary>
        /// <param name="arrayLenght">array length.</param>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue or array lenght less than one.</exception>
        /// <returns>integer array.</returns>
        public static int[] CreateIntArrayWithRandomElements(int arrayLenght, int minRandomValue, int maxRandomValue)
        {
            Helpers.IfValueLessThanDefinedException(value: arrayLenght, edgeValue: 1, nameof(arrayLenght));
            Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

            int[] array = new int[arrayLenght];

            for (int i = 0; i < arrayLenght; i++)
            {
                array[i] = GetRandomInteger(minRandomValue, maxRandomValue);
            }

            return array;
        }

        /// <summary>
        /// multiplies the array by a number.
        /// </summary>
        /// <param name="array">input Array.</param>
        /// <param name="number">number.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>Multiplied array by number.</returns>
        public static int[] MultipliesArrayByNumber(int[] array, int number)
        {
            Helpers.IfArrayIsNullOrEmptyException(array);

            var result = array.Select(i => i * number).ToArray();
            return result;
        }

        /// <summary>
        /// Generate a random integer value.
        /// </summary>
        /// <param name="minRandomValue">minimum random value.</param>
        /// <param name="maxRandomValue">maximum random value.</param>
        /// <exception cref="ArgumentException">if the maxRandomValue less than minRandomValue.</exception>
        /// <returns>random integer value.</returns>
        public static int GetRandomInteger(int minRandomValue, int maxRandomValue)
        {
            Helpers.IfValueLessThanDefinedException(value: maxRandomValue, edgeValue: minRandomValue, nameof(maxRandomValue));

            lock (random)
            {
                return random.Next(minRandomValue, maxRandomValue + 1);
            }
        }

        /// <summary>
        /// Sort input int array by ascending.
        /// </summary>
        /// <param name="array">input array.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>Sort array by ascending.</returns>
        public static int[] SortArrayByAscending(int[] array)
        {
            Helpers.IfArrayIsNullOrEmptyException(array);

            var result = array.OrderBy(x => x).ToArray();
            return result;
        }

        /// <summary>
        /// Calculate the average of the elements of an integer array.
        /// </summary>
        /// <param name="array">input array.</param>
        /// <exception cref="ArgumentNullException">If array is null.</exception>
        /// <exception cref="ArgumentException">If array is empty.</exception>
        /// <returns>average of the elements.</returns>
        public static double GetTheAverageOfTheElementsOfIntegerArray(int[] array)
        {
            Helpers.IfArrayIsNullOrEmptyException(array);

            var result = array.Average();
            return result;
        }

        /// <summary>
        /// Print array to console.
        /// </summary>
        /// <typeparam name="T">type or array.</typeparam>
        /// <param name="array">array.</param>
        /// <param name="title">title of array.</param>
        /// <exception cref="ArgumentNullException">If title or array is null.</exception>
        /// <exception cref="ArgumentException">If title or array is empty.</exception>
        public static void PrintArray<T>(T[] array, string title)
        {
            Helpers.IfStringNullOrEmptyException(title);
            Helpers.IfArrayIsNullOrEmptyException(array);

            lock (ConsoleLock)
            {
                Console.WriteLine($"\n{title}");
                for (int i = 0; i < array.Length; i++)
                {
                    Console.WriteLine($"\t{array[i]}");
                }
            }
        }

        /// <summary>
        /// Print message.
        /// </summary>
        /// <param name="message">message.</param>
        /// <exception cref="ArgumentNullException">If string is null.</exception>
        /// <exception cref="ArgumentException">If string is empty.</exception>
        public static void PrintMessage(string message)
        {
            Helpers.IfStringNullOrEmptyException(message);

            lock (ConsoleLock)
            {
                Console.WriteLine($"\n{message}");
            }
        }
    }
}
