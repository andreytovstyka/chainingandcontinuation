﻿// <copyright file="Program.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace ChainingAndContinuation
{
    /// <summary>
    /// Class programm.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        internal static void Main()
        {
            var result = ChainingAndContinuation.Demonstration(arrayLenght: 10, minRandomValue: 1, maxRandomValue: 10);
            Console.WriteLine($"\nResult {result}");
        }
    }
}