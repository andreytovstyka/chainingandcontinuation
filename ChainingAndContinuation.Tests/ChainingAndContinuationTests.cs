// <copyright file="ChainingAndContinuationTests.cs" company="Company">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ChainingAndContinuation.Tests
{
    using NUnit.Framework;

    /// <summary>
    /// ChainingAndContinuation Tests.
    /// </summary>
    [TestFixture]
    public class ChainingAndContinuationTests
    {
        private const int MultiplyByTen = 10;
        private static readonly int[] EmptyArray = Array.Empty<int>();
        private static readonly int[] ArrayFromTenToOne = new int[] { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void MultipliesArrayByNumber_EmptyArray_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.MultipliesArrayByNumber(EmptyArray, MultiplyByTen));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void MultipliesArrayByNumber_NullArray_ArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.MultipliesArrayByNumber(null, MultiplyByTen));
        }

        /// <summary>
        /// Checking the result of MultipliesArrayByNumber method.
        /// </summary>
        [Test]
        public void MultipliesArrayByNumber_Array_ReturnsArray()
        {
            var expected = new int[] { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };

            var act = ChainingAndContinuation.MultipliesArrayByNumber(ArrayFromTenToOne, MultiplyByTen);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void SortArrayByAscending_EmptyArray_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.SortArrayByAscending(EmptyArray));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void SortArrayByAscending_NullArray_ArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.SortArrayByAscending(null));
        }

        /// <summary>
        /// Checking the result of SortArrayByAscending method.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Array_ReturnsArray()
        {
            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var act = ChainingAndContinuation.SortArrayByAscending(ArrayFromTenToOne);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Checking if an exception will be thrown if an empty array is passed.
        /// </summary>
        [Test]
        public void GetTheAverageOfTheElementsOfIntegerArray_EmptyArray_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => ChainingAndContinuation.GetTheAverageOfTheElementsOfIntegerArray(EmptyArray));
        }

        /// <summary>
        /// Checking if an exception will be thrown if null is passed.
        /// </summary>
        [Test]
        public void GetTheAverageOfTheElementsOfIntegerArray_NullArray_ArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => ChainingAndContinuation.GetTheAverageOfTheElementsOfIntegerArray(null));
        }

        /// <summary>
        /// Checking the result of GetTheAverageOfTheElementsOfIntegerArray method.
        /// </summary>
        [Test]
        public void GetTheAverageOfTheElementsOfIntegerArray_Array_ReturnsArray()
        {
            var expected = 5.5;

            var act = ChainingAndContinuation.GetTheAverageOfTheElementsOfIntegerArray(ArrayFromTenToOne);

            Assert.That(act, Is.EqualTo(expected));
        }
    }
}